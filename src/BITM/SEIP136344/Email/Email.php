<?php


namespace App\Email;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;


class Email extends DB{

    public $id="";

    public $name="";

    public $email="";



    public function __construct(){

        parent::__construct();

    }

    public function index(){
        echo "Email found!";
    }
    public function setData($postVariabledata=NULL){

        if(array_key_exists('id',$postVariabledata)){
            $this->id = $postVariabledata['id'];
        }

        if(array_key_exists('name',$postVariabledata)){
            $this->name = $postVariabledata['name'];
        }

        if(array_key_exists('email',$postVariabledata)){
            $this->email = $postVariabledata['email'];
        }
    }

    public function store(){

        $arrData = array($this->name, $this->email);

        $sql = "Insert INTO email(name, email) VALUES(?,?)";


        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Inserted Successfully! :)");
        else
            Message::message("Failed! Data Has Not Been Inserted! :(");

        Utility::redirect('create.php');

    } //end of store method

}


